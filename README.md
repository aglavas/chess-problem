# Welcome

Chess Problem application solves unique configurations of different chess pieces across arbitrary dimensioned chessboards. Number of pieces is custom and their colors don't matter. This problem is further generalization of well-known n-queens (originally 8-queens) problem.

## Description

The task is to find all unique layouts on chessboard with arbitrary dimensions M*N where neither chess piece is in situation to attack or to be attacked by another one. It's necessary to provide the total number of such layouts together with layouts themselves. The color of chess piece doesn't matter and there are no paws among them.

Program takes as an input the next parameters:

* Chessboard dimensions M and N
* Chess pieces for which we are examining all possible layouts which comply with presented rules

Results are displayed in the file which is named after provided parameters with some basic info also available in the console where ChessProblem.jar file is ran from.

## Examples

For chessboard with dimensions 3*3 and for 3 Bishops and 1 King as chess pieces there are 4 unique layouts available:


```
#!javascript

BBB    B**    **B    *K*
***    B*K    K*B    ***
*K*    B**    **B    BBB
```


For chessboard with dimensions 5*4 and for 2 Rooks and 4 Bishops there are also 4 unique layouts available:


```
#!javascript

**B*B     ***BB     BB***     B*B**
R****     *R***     ***R*     ****R
*R***     R****     ****R     ***R*
***BB     **B*B     B*B**     BB***
```


## How does it work?

Application is built by using "sbt assembly" command. Executing this command builds ChessProblem.jar file in target directory. Application could be ran using one of the next three options:

*java -jar ChessProblem.jar --chessboard-description --pieces-description*

*java -jar ChessProblem.jar --chessboard-description_pieces-description*

*java -jar ChessProblem.jar --execution-algorithm[I, E, M] --chessboard-description --pieces-description*

For example:

*java -jar ChessProblem.jar M:6,N:6 Q:6*

*java -jar ChessProblem.jar M:6,N:6,Q:6*

*java -jar ChessProblem.jar M M:6,N:6 Q:6*


**Please note that choosing execution algorithm is an experimental feature so it's suggested to use default computation algorithm (the one marked by 'M'). Otherwise it couldn't be guaranteed good enough performances and stable computations.**

Execution algorithms would make more sense in the future, where each one has its own advantages and shortcomings. Currently, only 'M' algorithm could handle larger computations with solid performances. Algorithm 'I' wastes small, almost constant, amount of memory but with poor speed. Algorithm 'E' wastes a lot of memory, but it's closer to FP purity than any of the previous two and that's the only reason why it hasn't been scrapped yet.



For 7x7 chessboard and the next pieces: 2 queens, 2 kings, 2 bishops and 1 knight there's 3063828 unique configurations.


This example could be checked by the next command:

*java -jar ChessProblem.jar M M:7,N:7 Q:2,K:2,B:2,Kn:1*

As result of executing it we should get approximately the next values (for configuration Win8 64bit, i7, 3GHz, 12GB RAM - Asus laptop):


*Computation time: 40 +/- 10s (depending upon the other running processes).
Total number of unique configurations: 3063828.*

![console_10.png](https://bitbucket.org/repo/RKq5pE/images/4117796024-console_10.png)

All results are saved in the root folder of the running ChessProblem.jar file which has recognizable name, e.g. file with results from the first example would be named: 3x3_3xBishop_1xKing, etc.