assemblyJarName in assembly := "ChessProblem.jar"

name := "chess-problem"

version := "1.0"

scalaVersion := "2.11.7"

libraryDependencies ++= Seq(
  "org.scalatest" %% "scalatest" % "2.2.4" % "test",
  "org.scalaz.stream" %% "scalaz-stream" % "0.7.1",
  "ch.qos.logback" % "logback-classic" % "1.1.3",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.1.0"
)

resolvers += "Scalaz Bintray Repo" at "http://dl.bintray.com/scalaz/releases"

fork in run := true

javaOptions in run += "-Xmx8G"