object TestExternal2 {
  def bitwiseQueens(n: Long): Long = {
    var count = 0
    var nn = 0

    def bitwiseQueensScala: Long = {
      nn = (1 << n) - 1
      count = 0
      tryQ(0, 0, 0)
      count
    }
    def tryQ(ld: Long, cols: Long, rd: Long): Unit = {
      if (cols == nn) {
        count += 1
      } else {
        var poss = nn & ~(ld | cols | rd)
        while (poss != 0) {
          val bit = poss & -poss
          poss = poss - bit
          tryQ((ld | bit) << 1, cols | bit, (rd | bit) >> 1)
        }
      }
    }
    bitwiseQueensScala
  }
  bitwiseQueens(8)
}