import scala.annotation.tailrec

object TestExternal3 {
  def queens(n: Int): Array[Array[Int]] = {
    def placeQueens(k: Int): List[List[Int]] =
    if (k == 0) List(List())
    else for {
      queens <- placeQueens(k - 1)
      column <- List.range(1, n + 1)
      if isSafe(column, queens, 1)
    } yield column :: queens

    @tailrec
    def isSafe(col: Int, queens: List[Int], delta: Int): Boolean =
    queens match {
      case Nil => true
      case c :: rest => c != col && math.abs(c - col) != delta && isSafe(col, rest, delta + 1)
    }
    (placeQueens(n) map (_.toArray)).toArray
  }
  queens(8).length
}