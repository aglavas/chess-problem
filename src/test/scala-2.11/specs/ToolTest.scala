package specs

import org.agmonser.chess.pieces._
import org.agmonser.chess.tools.Bag
import org.scalatest.{Matchers, FlatSpec}

/**
 * Created by Aleksandar on 7/20/2015.
 */
class ToolTest extends FlatSpec with Matchers {
  "Bag functionality check " should "produce proper bags as result of operations upon the bag. " in {
    val b = Bag[ChessPiece]()
    val b1 = b.add(King())
    val b2 = b1.add(Queen())

    b.uniqueSize should be (0)
    b1.uniqueSize should be (1)
    b2.uniqueSize should be (2)

    val b3 = b2.add(Queen())
    b3(King()) should be (1)
    b3(Queen()) should be (2)
    b3.size should be (3)

    val b4 = b3.sort(_ > _)
    b4 should be (new Bag[ChessPiece] {override val bag = Map[ChessPiece, Int](Queen() -> 2, King() -> 1) })

    val b5 = b4.removeAll(Queen())
    b5 should be (new Bag[ChessPiece] {override val bag = Map[ChessPiece, Int](King() -> 1) })

    val b6 = b4.remove(Queen())
    b6 should be (new Bag[ChessPiece] {override val bag = Map[ChessPiece, Int](Queen() -> 1, King() -> 1) })

    val b7 = b4.add(Rook(), Knight(), Knight(), Bishop(), Rook(), Rook())
    b7 should be (new Bag[ChessPiece] {override val bag = Map[ChessPiece, Int](Queen() -> 2, King() -> 1, Rook() -> 3, Knight() -> 2, Bishop() -> 1) })

    val b8 = b7.sort(_ > _)
    b8 should be (new Bag[ChessPiece] {override val bag = Map[ChessPiece, Int](Queen() -> 2, Rook() -> 3, Bishop() -> 1, King() -> 1,  Knight() -> 2) })
  }
}
