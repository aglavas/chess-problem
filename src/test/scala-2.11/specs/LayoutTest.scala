package specs

import com.typesafe.scalalogging.LazyLogging
import org.agmonser.chess.layouts.Explorer
import org.agmonser.chess.pieces._
import org.agmonser.chess.tools.Bag
import org.scalatest.{Ignore, FlatSpec, Matchers}

/**
 * Created by Aleksandar on 7/19/2015.
 *
 * Testing deprecated method, but since it still exists in the code let it be tested as well.
 */
class LayoutTest extends FlatSpec with Matchers with LazyLogging {
  "3x3 layout check " should "produce proper list of available layouts " in {
    implicit val board = Chessboard(3, 3)
    val pieces = List[ChessPiece](King(), Rook(), King()).sortWith(_ > _)
    val layouts = Explorer.getLayouts(board, pieces)
    layouts.size should be (4)
  }

  "4x4 layout check " should "produce proper list of available layouts " in {
    implicit val board = Chessboard(4, 4)
    val pieces = List[ChessPiece](Rook(), Knight(), Knight(), Rook(), Knight(), Knight())
    val layouts = Explorer.getLayouts(board, pieces)
    layouts.size should be (8)
  }

  "5-queens check " should "produce proper list of available layouts " in {
    implicit val board = Chessboard(5, 5)
    val pieces = List[ChessPiece](Queen(), Queen(), Queen(), Queen(), Queen())
    val layouts = Explorer.getLayouts(board, pieces)
    layouts.size should be (10)
  }

  "6-queens check " should "produce proper list of available layouts " in {
    implicit val board = Chessboard(6, 6)
    val pieces = List[ChessPiece](Queen(), Queen(), Queen(), Queen(), Queen(), Queen())
    val layouts = Explorer.getLayouts(board, pieces)
    layouts.size should be (4)
  }
}

/**
 * This test class should contain unit tests that are usually too demanding but still necessary for verification of the
 * chess problem solution.
 */
@Ignore class LayoutDemandingTest extends FlatSpec with Matchers with LazyLogging {
  "7-queens check " should "produce proper list of available layouts " in {
    implicit val board = Chessboard(7, 7)
    val pieces = List[ChessPiece](Queen(), Queen(), Queen(), Queen(), Queen(), Queen(), Queen())
    val layouts = Explorer.getLayouts(board, pieces)
    layouts.size should be (40)
  }

  "8-queens check " should "produce proper list of available layouts " in {
    implicit val board = Chessboard(8, 8)
    val pieces = List[ChessPiece](Queen(), Queen(), Queen(), Queen(), Queen(), Queen(), Queen(), Queen())
    val layouts = Explorer.getLayouts(board, pieces)
    layouts.size should be (92)
  }

  "7x7 layout check " should "produce proper list of available layouts " in {
    implicit val board = Chessboard(7, 7)
    val pieces = List[ChessPiece](King(), King(), Queen(), Queen(), Bishop(), Bishop(), Knight())
    val layouts = Explorer.getLayouts(board, pieces)
    layouts.size should be (3063828)
  }

  "9-queens check " should "produce proper list of available layouts " in {
    implicit val board = Chessboard(9, 9)
    val pieces = List[ChessPiece](Queen(), Queen(), Queen(), Queen(), Queen(), Queen(), Queen(), Queen(), Queen())
    val layouts = Explorer.getLayouts(board, pieces)
    layouts.size should be (352)
  }
}