package specs

import org.agmonser.chess.pieces._
import org.scalatest._

/**
 * Created by Aleksandar on 7/18/2015.
 */
class MoveTest extends FlatSpec with Matchers {
  "Knight attacks method " should "produce proper attacked fields" in {
    val n = Knight()
    implicit val chessboard = Chessboard(6, 6)
    n.attacks(Position(0, 0)) should be (Array(1, 4, 2, 0, 0, 0))
    n.attacks(Position(0, 1)) should be (Array(2, 8, 5, 0, 0, 0))
    n.attacks(Position(0, 2)) should be (Array(4, 17, 10, 0, 0, 0))

    n.attacks(Position(1, 0)) should be (Array(4, 1, 4, 2, 0, 0))
    n.attacks(Position(1, 1)) should be (Array(8, 2, 8, 5, 0, 0))
    n.attacks(Position(1, 2)) should be (Array(17, 4, 17, 10, 0 ,0))

    n.attacks(Position(2, 0)) should be (Array(2, 4, 1, 4, 2, 0))
    n.attacks(Position(2, 1)) should be (Array(5, 8, 2, 8, 5, 0))
    n.attacks(Position(2, 2)) should be (Array(10, 17, 4, 17, 10, 0))

    n.attacks(Position(3, 3)) should be (Array(0, 20, 34, 8, 34, 20))

    n.attacks(Position(5, 5)) should be (Array(0, 0, 0, 16, 8, 32))
    n.attacks(Position(4, 4)) should be (Array(0, 0, 40, 4, 16, 4))
  }

  "Bishop attacks method " should "produce proper attacked fields" in {
    val b = Bishop()
    implicit val chessboard = Chessboard(6, 6)
    b.attacks(Position(0, 0)) should be (Array(1, 2, 4, 8, 16, 32))
    b.attacks(Position(0, 1)) should be (Array(2, 5, 8, 16, 32, 0))
    b.attacks(Position(0, 2)) should be (Array(4, 10, 17, 32, 0, 0))

    b.attacks(Position(1, 0)) should be (Array(2, 1, 2, 4, 8, 16))
    b.attacks(Position(1, 1)) should be (Array(5, 2, 5, 8, 16, 32))
    b.attacks(Position(1, 2)) should be (Array(10, 4, 10, 17, 32 ,0))

    b.attacks(Position(2, 0)) should be (Array(4, 2, 1, 2, 4, 8))
    b.attacks(Position(2, 1)) should be (Array(8, 5, 2, 5, 8, 16))
    b.attacks(Position(2, 2)) should be (Array(17, 10, 4, 10, 17, 32))

    b.attacks(Position(3, 3)) should be (Array(1, 34, 20, 8, 20, 34))

    b.attacks(Position(5, 5)) should be (Array(1, 2, 4, 8, 16, 32))
    b.attacks(Position(4, 4)) should be (Array(1, 2, 4, 40, 16, 40))
  }

  "Rook attacks method " should "produce proper attacked fields" in {
    val r = Rook()
    implicit val chessboard = Chessboard(6, 6)
    r.attacks(Position(0, 0)) should be (Array(63, 1, 1, 1, 1, 1))
    r.attacks(Position(0, 1)) should be (Array(63, 2, 2, 2, 2, 2))
    r.attacks(Position(0, 2)) should be (Array(63, 4, 4, 4, 4, 4))

    r.attacks(Position(1, 0)) should be (Array(1, 63, 1, 1, 1, 1))
    r.attacks(Position(1, 1)) should be (Array(2, 63, 2, 2, 2, 2))
    r.attacks(Position(1, 2)) should be (Array(4, 63, 4, 4, 4 ,4))

    r.attacks(Position(2, 0)) should be (Array(1, 1, 63, 1, 1, 1))
    r.attacks(Position(2, 1)) should be (Array(2, 2, 63, 2, 2, 2))
    r.attacks(Position(2, 2)) should be (Array(4, 4, 63, 4, 4, 4))

    r.attacks(Position(3, 3)) should be (Array(8, 8, 8, 63, 8, 8))

    r.attacks(Position(5, 5)) should be (Array(32, 32, 32, 32, 32, 63))
    r.attacks(Position(4, 4)) should be (Array(16, 16, 16, 16, 63, 16))
  }

  "Queen attacks method " should "produce proper attacked fields" in {
    val q = Queen()
    implicit val chessboard = Chessboard(6, 6)
    q.attacks(Position(0, 0)) should be (Array(63, 3, 5, 9, 17, 33))
    q.attacks(Position(0, 1)) should be (Array(63, 7, 10, 18, 34, 2))
    q.attacks(Position(0, 2)) should be (Array(63, 14, 21, 36, 4, 4))

    q.attacks(Position(1, 0)) should be (Array(3, 63, 3, 5, 9, 17))
    q.attacks(Position(1, 1)) should be (Array(7, 63, 7, 10, 18, 34))
    q.attacks(Position(1, 2)) should be (Array(14, 63, 14, 21, 36, 4))

    q.attacks(Position(2, 0)) should be (Array(5, 3, 63, 3, 5, 9))
    q.attacks(Position(2, 1)) should be (Array(10, 7, 63, 7, 10, 18))
    q.attacks(Position(2, 2)) should be (Array(21, 14, 63, 14, 21, 36))

    q.attacks(Position(3, 3)) should be (Array(9, 42, 28, 63, 28, 42))

    q.attacks(Position(5, 5)) should be (Array(33, 34, 36, 40, 48, 63))
    q.attacks(Position(4, 4)) should be (Array(17, 18, 20, 56, 63, 56))
  }

  "King attacks method " should "produce proper attacked fields" in {
    val k = King()
    implicit val chessboard = Chessboard(6, 6)
    k.attacks(Position(0, 0)) should be (Array(3, 3, 0, 0, 0, 0))
    k.attacks(Position(0, 1)) should be (Array(7, 7, 0, 0, 0, 0))
    k.attacks(Position(0, 2)) should be (Array(14, 14, 0, 0, 0, 0))

    k.attacks(Position(1, 0)) should be (Array(3, 3, 3, 0, 0, 0))
    k.attacks(Position(1, 1)) should be (Array(7, 7, 7, 0, 0, 0))
    k.attacks(Position(1, 2)) should be (Array(14, 14, 14, 0, 0, 0))

    k.attacks(Position(2, 0)) should be (Array(0, 3, 3, 3, 0, 0))
    k.attacks(Position(2, 1)) should be (Array(0, 7, 7, 7, 0, 0))
    k.attacks(Position(2, 2)) should be (Array(0, 14, 14, 14, 0, 0))

    k.attacks(Position(3, 3)) should be (Array(0, 0, 28, 28, 28, 0))

    k.attacks(Position(5, 5)) should be (Array(0, 0, 0, 0, 48, 48))
    k.attacks(Position(4, 4)) should be (Array(0, 0, 0, 56, 56, 56))
  }
}
