package org.agmonser.chess.exceptions

/**
 * Created by Aleksandar on 7/18/2015.
 */
case class IllegalComparison(msg: String) extends Exception(msg)