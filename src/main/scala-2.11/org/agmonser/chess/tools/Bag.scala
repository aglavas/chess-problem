package org.agmonser.chess.tools

import org.agmonser.chess.pieces.ChessPiece

/**
 * Created by Aleksandar on 7/20/2015.
 *
 * Data structure with counter for each saved item. Implemented as monad for easier iteration and combinator features.
 */
case class Bag[A <: Ordered[A]](items: A*) { self =>
  protected val bag: Map[A, Int] = addItemsToBag(Map[A, Int](), items)

  /**
   * Adds new item or increases counter by one if item already exists.
   * 
   * @param item element to be added
   * @return bag with element added
   */
  private def addItem(item: A): Bag[A] = new Bag[A]{ override val bag: Map[A, Int] = addItemToBag(self.bag, item) }

  /**
   * Add new items and increase counter by one for each item.
   *
   * @param items to be added
   * @return bag with elements added
   */
  def add(items: A*): Bag[A] = {
    var newBag = self
    for {item <- items} newBag = newBag.addItem(item)
    newBag
  }

  /**
   * Private helper method for updating map in terms of bag
   * @param b map simulating bag
   * @param item item to be updated with
   * @return updated map
   */
  private def addItemToBag(b: Map[A, Int], item: A): Map[A, Int] =
    if (b.exists(p => p._1 == item)) b + (item -> (b(item) + 1)) else b + (item -> 1)

  /**
   * Private helper method for updating map in terms of bag with multiple items
   * @param b map simulating bag
   * @param items elements to be updated with
   * @return updated map
   */
  private def addItemsToBag(b: Map[A, Int], items: Seq[A]): Map[A, Int] = {
    var newBag = b
    for {item <- items} newBag = addItemToBag(newBag, item)
    newBag
  }

  /**
   * Removes single item from bag.
   *
   * @param item element to be removed
   * @return bag with element removed
   */
  def remove(item: A): Bag[A] =
    if (bag.exists(p => p._1 == item)) {
      if (bag(item) == 1) new Bag[A]{ override val bag = self.bag - item}
      else new Bag[A] { override val bag = self.bag + (item -> (self.bag(item) - 1)) }
    } else self
  
  /**
   * Removes all items of the given value.
   *
   * @param item elements to be removed
   * @return bag with elements removed
   */
  def removeAll(item: A): Bag[A] = if (bag.exists(p => p._1 == item)) new Bag[A] { override val bag = self.bag - item } else self

  /**
   * Returns number of items in the bag.
   *
   * @param item element to be checked
   * @return number of certain elements
   */
  def apply(item: A): Int = if (bag.exists(p => p._1 == item)) bag(item) else 0

  /**
   * Sorts bag based upon the sorting criteria.
   *
   * @param f sorting criteria
   * @return sorted bag
   */
  def sort(f: (A, A) => Boolean): Bag[A] = new Bag[A] { override val bag = Map(self.bag.toSeq.sortWith((l,r) => f(l._1, r._1)): _*) }

  /**
   * Get bag head.
   *
   * @return head
   */
  def head: (A, Int) = bag.head

  /**
   * Get bag tail.
   *
   * @return tail
   */
  def tail: Bag[A] = new Bag[A] { override val bag = self.bag.tail }

  /**
   * Size of the unique elements in the bag.
   *
   * @return bag size
   */
  def uniqueSize: Int = bag.size

  /**
   * Size of the bag.
   *
   * @return bag size.
   */
  def size: Int = bag.foldLeft(0){ case (acc, (piece, size)) => acc + size }
  
  /**
   * flatMap implementation for the bag.
   *
   * @param f transformation function
   * @tparam B bag type to convert to
   * @return transformed bag
   */
  def flatMap[B <: Ordered[B]](f: A => Bag[B]): Bag[B] = new Bag[B] { override val bag = self.bag.flatMap(p => f(p._1).bag) }

  /**
   * map implementation for the bag.
   *
   * @param f transformation function
   * @tparam B bag type to convert to
   * @return transformed bag
   */
  def map[B <: Ordered[B]](f: A => B): Bag[B] = flatMap(a => unit(f(a)))

  /**
   * foreach implementation for the bag.
   *
   * @param f handler function
   * @tparam B handler result type
   */
  def foreach[B](f: ((A, Int)) => B): Unit = bag.foreach(f)

  /**
   * unit implementation for the bag.
   *
   * @param a item for which unit is applied
   * @tparam A bag type
   * @return bag created for the given item
   */
  def unit[A <: Ordered[A]](a: A): Bag[A] = new Bag[A] { override val bag = Map(a -> 1) }

  /**
   * String representation of the bag data structure.
   *
   * @return string
   */
  override def toString: String = {
    val s = {for {
      (piece, n) <- bag
    } yield {
      s"${n}x$piece"
    }} mkString ","
    s
  }
}

/**
 * Bag companion object.
 */
object Bag {
  implicit def bagToList(b: Bag[ChessPiece]): List[ChessPiece] =
  {for {
    (k, v) <- b.bag
  } yield List.fill(v)(k)}.toList.flatten
}