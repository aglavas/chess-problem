package org.agmonser.chess.tools

import org.agmonser.chess.pieces.{ChessPiece, Chessboard}

/**
 * Created by Aleksandar on 7/22/2015.
 */
case class Arguments(method: String, chessboard: Chessboard, pieces: Bag[ChessPiece], original: String)
