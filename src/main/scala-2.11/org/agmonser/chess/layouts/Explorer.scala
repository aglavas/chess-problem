package org.agmonser.chess.layouts

import com.typesafe.scalalogging.LazyLogging
import org.agmonser.chess.pieces.ChessPiece._
import org.agmonser.chess.pieces.{Position, PiecePosition, ChessPiece, Chessboard}
import org.agmonser.chess.tools.Bag

/**
 * Created by Aleksandar on 7/19/2015.
 *
 * Helper object which implements method for retrieving all available chess pieces combinations so they don't
 * attack each other.
 */
object Explorer extends LazyLogging {
  /**
   * This one is here only for 'historic' reasons. It was just starting point of developing basic idea -
   * it has no real value and it's not even included as available algorithm for solving the problem.
   *
   * Method which examines all possible combinations of chess pieces which don't attack each other.
   *
   * @param board chessboard dimensions
   * @param pieces chess pieces to be examined
   * @return list of all chess pieces positioned on the chessboard
   */
  @deprecated
  def getLayouts(board: Chessboard, pieces: List[ChessPiece]): Set[Layout] = {
    /**
     * Helper method to iterate through all combinations for the given list of chess pieces.
     *
     * @param squares current squares layout
     * @param pieces remaining chess pieces
     * @return list of chess pieces on the chessboard
     */
    def explore(squares: ChessboardSquares, pieces: List[ChessPiece], piecesOnBoard: Layout): Set[Layout] =
      pieces match {
        case Nil => Set(new Layout())
        case piece :: ps => {
          implicit val implBoard = board
          for {
            i <- 0 until board.height
            j <- 0 until board.width
            pp = PiecePosition(piece, Position(i, j))
            if (squares.isSafe(piecesOnBoard, pp))
            pops <- explore(piece.mergeAttack(squares)(Position(i, j)), ps, piecesOnBoard + pp)
          } yield pops + pp
        }.toSet
      }
    explore(IndexedSeq.fill(board.height)(0L), pieces.sortWith(_ > _), new Layout()).filter(_.size == pieces.length)
  }

  /**
   * This one is pure FP implementation but it shouldn't be used yet. Symbol for activating this algorithm is 'E'.
   *
   * Method which examines all possible combinations of chess pieces which don't attack each other.
   *
   * @param board chessboard dimensions
   * @param pieces chess pieces to be examined
   * @return list of all chess pieces positioned on the chessboard
   */
  def getLayoutsEnhanced(board: Chessboard, pieces: List[ChessPiece]): Set[Layout] = {
    /**
     * Helper method to iterate through all combinations for the given list of chess pieces.
     *
     * @param squares current squares layout
     * @param pieces remaining chess pieces
     * @return list of chess pieces on the chessboard
     */
    def explore(squares: ChessboardSquares, pieces: List[ChessPiece], piecesOnBoard: Layout): Set[Layout] =
      pieces match {
        case Nil => Set(new Layout())
        case piece :: ps => {
          implicit val implBoard = board
          for {
            pos <- squares.freeSquaresForPiece(implBoard, getOccupiedPositions(board, piecesOnBoard), piece)
            pp = PiecePosition(piece, pos)
            pops <- explore(piece.mergeAttack(squares)(pos), ps, piecesOnBoard + pp)
          } yield pops + pp
        }.toSet
      }
    explore(IndexedSeq.fill(board.height)(0L), pieces.sortWith(_ > _), new Layout()).filter(_.size == pieces.length)
  }

  /**
   * This one is default algorithm and it should be used for examining unique configurations. Symbol for activating
   * this algorithm explicitly is 'M'.
   *
   * Method which examines all possible combinations of chess pieces which don't attack each other.
   *
   * @param board chessboard dimensions
   * @param pieces chess pieces to be examined
   * @return list of all chess pieces positioned on the chessboard
   */
  def getLayouts(board: Chessboard, pieces: Bag[ChessPiece]): IndexedSeq[Layout] = {
    /**
     * Helper method to iterate through all combinations for the given list of chess pieces.
     *
     * @param squares current squares layout
     * @param pieces remaining chess pieces
     * @param piecesOnBoard already positioned pieces
     * @return list of chess pieces on the chessboard
     */
    def explore(squares: ChessboardSquares, pieces: Bag[ChessPiece], piecesOnBoard: Layout): IndexedSeq[Layout] =
      if (pieces.uniqueSize == 0) IndexedSeq(new Layout())
      else {
        val (piece, ps) = (pieces.head, pieces.tail)
        for {
          (calcSquares, combs) <- squares.getMultiCombinations(board, piecesOnBoard, piece._1, piece._2)
          pops <- explore(calcSquares, ps, combs)
        } yield {
          pops ++ combs
        }
      }
    explore(IndexedSeq.fill(board.height)(0L), pieces.sort(_ > _), new Layout())
  }

  /**
   * This one is not invasive method which wastes the smallest amount of memory. Anyway, it takes significantly more
   * time for evaluating more complex tasks so it's not suggested to be used. Symbol for activating this algorithm is 'I'.
   *
   * Method which examines all possible combinations of chess pieces which don't attack each other.
   *
   * @param board chessboard dimensions
   * @param pieces chess pieces to be examined
   * @return list of all chess pieces positioned on the chessboard
   */
  def getLayoutsInversed(board: Chessboard, pieces: List[ChessPiece]): Set[Layout] = {
    // I don't like it, but it served as test sample to check improvements upon performances and memory footprint
    // for tracking layout states across iterations
    var layouts = Set[Layout]()
    var topLimit = 0
    /**
     * Helper method to iterate through all combinations for the given list of chess pieces.
     *
     * @param squares current squares layout
     * @param pieces remaining chess pieces
     * @return sets of chess pieces on the chessboard
     */
    def explore(squares: ChessboardSquares, pieces: List[ChessPiece], piecesOnBoard: Layout): Unit =
    pieces match {
      case Nil =>
      case head :: tail =>
        implicit val implBoard = board
        topLimit = Math.max(topLimit, piecesOnBoard.size)
        for {
          pos <- squares.freeSquaresForPiece(implBoard, getOccupiedPositions(board, piecesOnBoard), head)
          pp = PiecePosition(head, pos)
          layout = piecesOnBoard + pp
          if !layouts.contains(layout)
        } {
          layouts = layouts + layout
          layouts = layouts.filter(_.size > topLimit)
          explore(head.mergeAttack(squares)(pos), tail, piecesOnBoard + pp)
        }
    }
    explore(IndexedSeq.fill(board.height)(0L), pieces.sortWith(_ > _), new Layout())
    layouts
  }
}
