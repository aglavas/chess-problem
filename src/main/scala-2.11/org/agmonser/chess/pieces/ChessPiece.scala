package org.agmonser.chess.pieces

import com.typesafe.scalalogging.LazyLogging
import org.agmonser.chess.exceptions.IllegalComparison
import org.agmonser.chess.pieces.ChessPiece.ChessboardSquares
import scala.collection.immutable.HashSet

/**
 * Created by Aleksandar on 7/18/2015.
 *
 * Represents piece in chess game.
 */
sealed trait ChessPiece extends Ordered[ChessPiece] with LazyLogging { self =>
  /**
   * Comparing pieces weights. Instead of introducing weight value for each piece we are comparing them directly
   * to preserve clean interface for chess pieces. We skipped weight value as attribute to decrease as much as possible
   * memory footprint - that's the reason why we have a little bit more complicated 'compare' method without losing in
   * performances.
   *
   * @param other piece to compare with
   * @return comparison result
   */
  override def compare(other: ChessPiece): Int = (self, other) match {
    // first eliminate identity comparisons to avoid unnecessary sorting permutations
    case (Queen(), Queen()) | (Rook(), Rook()) | (Bishop(), Bishop()) | (King(), King()) | (Knight(), Knight()) => 0
    // comparing different pieces
    case (Queen(), _) => 1
    case (Rook(), Queen()) => -1
    case (Rook(), _) => 1
    case (Bishop(), Queen()) | (Bishop(), Rook()) => -1
    case (Bishop(), _) | (King(), Knight()) => 1
    case (King(), _) | (Knight(), _) => -1
    // default case should never be reached
    case _ => throw IllegalComparison(s"Illegal comparison of pieces $self and $other.")
  }

  /**
   * Higher order function which represents attack of the piece from certain position to the rest of the chessboard.
   *
   * @return function which represents attack of the piece from certain position on chessboard
   */
  def attacks: Position => ChessboardSquares

  /**
   * Simulate attack on board returning resulting board.
   *
   * @return resulting board
   */
  def mergeAttack: ChessboardSquares => Position => ChessboardSquares = board => pos => joinBoards(board, self.attacks(pos))

  /**
   * Limiting threatening radius for the self.
   *
   * @param limit radius size
   * @return chess piece with limited threatening radius compared to original piece
   */
  def limitedPiece(limit: Int): ChessPiece = new ChessPiece {
    /**
     * Higher order function which represents attack of the piece from certain position to the rest of the chessboard.
     *
     * @return function which represents attack of the piece from certain position on chessboard
     */
    override def attacks: (Position) => ChessboardSquares = pos => {
      if (limit > 0) {
        val mask = (1L << ((limit << 1) + 1)) - 1
        val lowLimit = if (pos.rank - limit < 0) 0 else pos.rank - limit
        val highLimit = if (pos.rank + limit >= pos.board.height) pos.board.height - 1 else pos.rank + limit
        val cb = self.attacks(pos)
        val board = Array.fill(pos.board.height)(0L)
        for (i <- lowLimit to highLimit) {
          board(i) = {cb(i) & {if (pos.file - limit > 0) mask << pos.file - limit else mask >> limit - pos.file}}
        }
        board
      } else
        self.attacks(pos)
    }
  }

  /**
   * Composing new piece based upon attacking power of self and another chess pieces.
   *
   * @param cp another chess piece
   * @return chess piece with combo power
   */
  def compose(cp: ChessPiece): ChessPiece = new ChessPiece {
    /**
     * Higher order function which represents attack of the piece from certain position to the rest of the chessboard.
     *
     * @return function which represents attack of the piece from certain position on chessboard
     */
    override def attacks: Position => ChessboardSquares = pos => joinBoards(self.attacks(pos), cp.attacks(pos))
  }

  /**
   * Helper function for joining two boards and returning result.
   *
   * @param left board
   * @param right board
   * @return resulting board
   */
  private def joinBoards(left: ChessboardSquares, right: ChessboardSquares): ChessboardSquares =
    for {
      i <- 0 until left.length
    } yield left(i) | right(i)
}

/**
 * ChessPiece companion object.
 */
object ChessPiece extends LazyLogging {
  type ChessboardSquares = IndexedSeq[Long]
  type Layout = HashSet[PiecePosition]

  /**
   * Helper method which returns occupied squares by pieces.
   *
   * @param board chessboard
   * @param piecesPositions pieces on their positions
   * @return occupied chessboard
   */
  def getOccupiedPositions(board: Chessboard, piecesPositions: Layout): ChessboardSquares = {
    val pieces = Array.fill(board.height)(0L)
    for (pp <- piecesPositions) pieces(pp.pos.rank) |= (1L << pp.pos.file)
    pieces
  }

  /**
   * Extending ChessboardSquares with helper methods for finding/checking positions with bit manipulation.
   *
   * @param cbs chessboard squares
   */
  implicit class ChessboardImprovements(val cbs: ChessboardSquares) {
    /**
     * Find positions for the all free squares on chessboard.
     *
     * @param board chessboard
     * @return all free positions
     */
    def freeSquares(board: Chessboard): IndexedSeq[Position] = {
      implicit val implBoard = board
      val mask = (1L << board.width) - 1
      for {
        (row, i) <- cbs.zipWithIndex
        if (row & mask) != mask
        freeSquares = ~row & mask
        j <- 0 until board.width
        if ((freeSquares >> j) & 1L) == 1L
      } yield Position(i, j)
    }

    /**
     * Checks whether two chessboard have common squares
     *
     * @param attacked chessboard
     * @return true if two chessboards are not in collision otherwise false
     */
    def checkPositions(attacked: ChessboardSquares): Boolean = {
      (cbs zip attacked).forall{ case (row, attack) => (row & attack) == 0L }
    }

    /**
     * Find positions of the free squares on chessboard for the given piece and constellation of the other pieces.
     *
     * @param board chessboard
     * @param piecesPositions existing pieces on their positions
     * @param piece examined piece
     * @return all free positions
     */
    def freeSquaresForPiece(board: Chessboard, piecesPositions: ChessboardSquares, piece: ChessPiece):  IndexedSeq[Position] = {
      implicit val implBoard = board
      val mask = (1L << board.width) - 1
      for {
        (row, i) <- cbs.zipWithIndex
        if (row & mask) != mask
        freeSquares = ~row & mask
        j <- 0 until board.width
        if ((freeSquares >> j) & 1L) == 1L
        pp = Position(i, j)
        if piecesPositions.checkPositions(piece.attacks(pp))
      } yield pp
    }

    /**
     * Find positions of the free squares on chessboard for the given piece and constellation of the other pieces.
     *
     * @param board chessboard
     * @param piecesPositions existing pieces on their positions
     * @param piece examined piece
     * @param excluded excluded positions
     * @return all free positions
     */
    def freeSquaresForPieceWithExclusion(board: Chessboard, piecesPositions: ChessboardSquares, piece: ChessPiece,
                                         excluded: Layout):  IndexedSeq[Position] = {
      implicit val implBoard = board
      val mask = (1L << board.width) - 1
      for {
        (row, i) <- cbs.zipWithIndex
        if (row & mask) != mask
        freeSquares = ~row & mask
        j <- 0 until board.width
        if ((freeSquares >> j) & 1L) == 1L
        pp = Position(i, j)
        if piecesPositions.checkPositions(piece.attacks(pp))
        if !excluded.contains(PiecePosition(piece, pp))
      } yield pp
    }

    /**
     * Checks position for the current chessboard.
     *
     * @param pos position to be examined on the chessboard
     * @return true if position is not attacked, otherwise returns false
     */
    def checkPosition(pos: Position): Boolean = ((cbs(pos.rank) >> pos.file) & 1) == 0

    /**
     * Checks whether the position is safe for the given chess piece on the given position for the constellation with
     * other chess pieces on their positions.
     *
     * @param piecesOnPositions existing pieces on their positions
     * @param pieceOnPosition current piece on checked position
     * @return true if position is not compromised, otherwise returns false
     */
    def isSafe(piecesOnPositions: Layout, pieceOnPosition: PiecePosition): Boolean = {
      if (checkPosition(pieceOnPosition.pos)) {
        val attacked = pieceOnPosition.piece.attacks(pieceOnPosition.pos)
        (for(pop <- piecesOnPositions; if (!attacked.checkPosition(pop.pos))) yield false).size <= 0
      } else false
    }

    /**
     * Returns all the unique configurations for the given multiset of the same piece in constellation with the existing
     * items on chessboard.
     *
     * @param piecesOnPositions existing pieces on their positions
     * @param piece type of piece
     * @param n number of pieces
     * @return unique configurations for the multi pieces
     */
    def getMultiCombinations(board: Chessboard, piecesOnPositions: Layout, piece: ChessPiece, n: Int):
    IndexedSeq[(ChessboardSquares, Layout)] = {
      // I don't like it, but it's been efficient way to track redundant states through iterations to improve performances
      val excludedLayouts = scala.collection.mutable.HashSet[Layout]()
      /**
       * Helper method to iterate through all combinations for the given list of chess pieces.
       *
       * @param squares current squares layout
       * @param pieces remaining chess pieces
       * @return sets of chess pieces on the chessboard
       */
      def explore(squares: ChessboardSquares, pieces: List[ChessPiece], piecesOnBoard: Layout): IndexedSeq[(ChessboardSquares, Layout)] =
        pieces match {
          case Nil =>
            IndexedSeq[(ChessboardSquares, Layout)]((squares, piecesOnBoard))
          case piece :: ps => {
            implicit val implBoard = board
            for {
              pos <- squares.freeSquaresForPiece(implBoard, getOccupiedPositions(board, piecesOnBoard), piece)
              pp = PiecePosition(piece, pos)
              layout = piecesOnBoard + pp
              if !excludedLayouts.contains(layout)
              _ = excludedLayouts += layout
              pops <- explore(piece.mergeAttack(squares)(pos), ps, layout)
            } yield (pops._1, pops._2 + pp)
          }
        }
      explore(cbs, List.fill(n)(piece).sortWith(_ > _), piecesOnPositions)
    }
  }

  /**
   * Extending Layout with helper methods for better printing.
   *
   * @param layout pieces configuration on chessboard
   */
  implicit class LayoutImprovements(val layout: Layout) {
    /**
     * String representation of layout configuration.
     *
     * @return layout string representation
     */
     def asString: String = {
      if (layout.size > 0) {
        val board = layout.head.pos.board
        val squares = Array.fill(board.height, board.width)('*')
        for {
          pp <- layout
        } {
          val mark = pp.piece match {
            case Queen() => 'Q'
            case King() => 'K'
            case Bishop() => 'B'
            case Rook() => 'R'
            case Knight() => 'N'
            case _ => '*'
          }
          squares(pp.pos.rank)(pp.pos.file) = mark
        }
        squares.map(_.mkString("")).mkString("----------------------------\n", "\n", "")
      } else ""
    }
  }
}

/**
 * King chess piece representation.
 */
case class King() extends ChessPiece {
  /**
   * Higher order function which represents attack of the piece from certain position to the rest of the chessboard.
   *
   * @return function which represents attack of the piece from certain position on chessboard
   */
  override def attacks: Position => ChessboardSquares = (Queen() limitedPiece 1) attacks

  /**
   * Piece string representation.
   *
   * @return string
   */
  override def toString: String = "King"
}
/**
 * Queen chess piece representation.
 */
case class Queen() extends ChessPiece {
  /**
   * Higher order function which represents attack of the piece from certain position to the rest of the chessboard.
   *
   * @return function which represents attack of the piece from certain position on chessboard
   */
  override def attacks: Position => ChessboardSquares = (Rook() compose Bishop()) attacks

  /**
   * Piece string representation.
   *
   * @return string
   */
  override def toString: String = "Queen"
}
/**
 * Rook chess piece representation.
 */
case class Rook() extends ChessPiece {
  /**
   * Higher order function which represents attack of the piece from certain position to the rest of the chessboard.
   *
   * @return function which represents attack of the piece from certain position on chessboard
   */
  override def attacks: Position => ChessboardSquares = pos => {
    val board = Array.fill(pos.board.height)(0L)
    if (pos.rank < pos.board.height && pos.file < pos.board.width && pos.rank >= 0 && pos.file >= 0) {
      val hMask = (1L << pos.board.width) - 1
      val vMask = 1L << pos.file
      for (i <- 0 until pos.board.height) {
        if (i == pos.rank) board(i) = hMask
        else board(i) = vMask
      }
    }
    board
  }

  /**
   * Piece string representation.
   *
   * @return string
   */
  override def toString: String = "Rook"
}
/**
 * Bishop chess piece representation.
 */
case class Bishop() extends ChessPiece {
  /**
   * Higher order function which represents attack of the piece from certain position to the rest of the chessboard.
   *
   * @return function which represents attack of the piece from certain position on chessboard
   */
  override def attacks: Position => ChessboardSquares = pos => {
    val board = Array.fill(pos.board.height)(0L)
    if (pos.rank < pos.board.height && pos.file < pos.board.width) {
      for (i <- 0 until pos.board.height; rd = (pos.file + pos.rank) - i; ld = (pos.file - pos.rank) + i) {
        board(i) = {{if (rd >= 0 && rd < pos.board.width) 1L << rd else 0} | {if (ld < pos.board.width && ld >= 0) 1L << ld else 0}}
      }
    }
    board
  }

  /**
   * Piece string representation.
   *
   * @return string
   */
  override def toString: String = "Bishop"
}
/**
 * Knight chess piece representation.
 */
case class Knight() extends ChessPiece {
  /**
   * Higher order function which represents attack of the piece from certain position to the rest of the chessboard.
   *
   * @return function which represents attack of the piece from certain position on chessboard
   */
  override def attacks: Position => ChessboardSquares = pos => {
    val board = Array.fill(pos.board.height)(0L)
    if (pos.rank < pos.board.height && pos.file < pos.board.width) {
      if (pos.rank - 1 >= 0) board(pos.rank - 1) =
        {{if (pos.file > 2) 17L << pos.file - 2 else 17L >> 2 - pos.file} & {(1L << pos.board.width) - 1L}}
      if (pos.rank - 2 >= 0) board(pos.rank - 2) =
        {{if (pos.file > 0) 5L << pos.file - 1 else 5L >> 1 - pos.file} & {(1L << pos.board.width) - 1L}}
      if (pos.rank + 1 < pos.board.height) board(pos.rank + 1) =
        {{if (pos.file > 2) 17L << pos.file - 2 else 17L >> 2 - pos.file} & {(1L << pos.board.width) - 1L}}
      if (pos.rank + 2 < pos.board.height) board(pos.rank + 2) =
        {{if (pos.file > 0) 5L << pos.file - 1 else 5L >> 1 - pos.file} & {(1L << pos.board.width) - 1L}}
      board(pos.rank) = 1 << pos.file
    }
    board
  }

  /**
   * Piece string representation.
   *
   * @return string
   */
  override def toString: String = "Knight"
}