package org.agmonser.chess.pieces

/**
 * Created by Aleksandar on 7/19/2015.
 *
 * Chessboard with size defined.
 *
 * @param height chessboard height - N dimension
 * @param width chessboard width - M dimension
 */
case class Chessboard(height: Int, width: Int)

/**
 * Represents position on chessboard.
 *
 * @param rank row position
 * @param file column position
 */
case class Position(rank: Int, file: Int)(implicit val board: Chessboard) {
  // this pre-condition check has been commented out due to significant loss in performances and the fact that
  // all illegal positions compared to the Chessboard dimensions have been ignored in calculation algorithm.
  //require(rank >= 0 && rank < board.height && file >= 0 && file < board.width)
}

/**
 * Position of the chess piece on chessboard.
 *
 * @param piece chess piece
 * @param pos piece position
 */
case class PiecePosition(piece: ChessPiece, pos: Position)
