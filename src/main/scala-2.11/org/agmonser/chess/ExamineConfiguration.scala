package org.agmonser.chess

import java.util.Date

import com.typesafe.scalalogging.LazyLogging
import org.agmonser.chess.layouts.Explorer
import org.agmonser.chess.pieces.ChessPiece._
import org.agmonser.chess.pieces._
import org.agmonser.chess.tools.{Arguments, Bag}

import scalaz.concurrent.Task
import scalaz.stream._

/**
 * Created by Aleksandar on 7/22/2015.
 *
 * Application object which reads arguments from command line and runs configuration evaluation for input params.
 */
object ExamineConfiguration extends App with LazyLogging {
  /**
   * Reads arguments from command line.
   *
   * @return arguments object
   */
  private def readArguments: Arguments = args.toList match {
    case m :: cbArg :: cpArg :: Nil =>
      val method = m match {
        case "E" | "I" | "M" => m
        case _ => "M"
      }
      val cbConfigs = getConfigs(cbArg)
      val cpConfigs = getConfigs(cpArg)
      val original = s"${cbConfigs("M")}x${cbConfigs("N")}_${cpArg.replace(":", "")}}"
      Arguments(method, Chessboard(cbConfigs("N"), cbConfigs("M")), convertToBag(cpConfigs), original)
    case cbArg :: cpArg :: Nil =>
      val cbConfigs = getConfigs(cbArg)
      val cpConfigs = getConfigs(cpArg)
      val original = s"${cbConfigs("M")}x${cbConfigs("N")}_${cpArg.replace(":", "")}}"
      Arguments("M", Chessboard(cbConfigs("N"), cbConfigs("M")), convertToBag(cpConfigs), original)
    case arg :: Nil =>
      val configs = getConfigs(arg)
      val secondPart = arg.substring(arg.indexOf(",", arg.indexOf(",") + 1) + 1).replace(":", "")
      val original = s"${configs("M")}x${configs("N")}_$secondPart}"
      Arguments("M", Chessboard(configs("N"), configs("M")), convertToBag(configs), original)
    case _ => throw new IllegalArgumentException("Illegal arguments as input for chess problem.")
  }

  /**
   * Parses configuration from input string
   *
   * @param arguments arguments contained in string object
   * @return map with arguments
   */
  private def getConfigs(arguments: String): Map[String, Int] = arguments.replace("\"", "").replace(" ", "").split(",").map{pair => {
    val p = pair.split(":")
    (p(0), p(1).toInt)
  }}.toMap

  /**
   * Converts map with chess pieces into the bag
   *
   * @param m map to be converted
   * @return bag object
   */
  private def convertToBag(m: Map[String, Int]): Bag[ChessPiece] = {
    val b: Map[ChessPiece, Int] = m.filter{case (k, v) => k != "N" && k != "M"}.map{ case (k, v) => k match {
      case "Q" => Queen() -> v
      case "R" => Rook() -> v
      case "B" => Bishop() -> v
      case "K" => King() -> v
      case "Kn" => Knight() -> v
      case _ => throw new IllegalArgumentException("Illegal argument as chess piece description.")
    }}
    new Bag[ChessPiece]{ override val bag = b }
  }

  val setup = readArguments
  val time = new Date()
  val layouts = setup.method match {
    case "E" => Explorer.getLayoutsEnhanced(setup.chessboard, Bag.bagToList(setup.pieces))
    case "I" => Explorer.getLayoutsInversed(setup.chessboard, Bag.bagToList(setup.pieces))
    case "M" => Explorer.getLayouts(setup.chessboard, setup.pieces)
    case _ => Explorer.getLayouts(setup.chessboard, setup.pieces)
  }
  val calcTime = (new Date().getTime - time.getTime)/1000f
  val filename = s"${setup.chessboard.width}x${setup.chessboard.height}_${setup.pieces.toString.replace(",", "_")}.txt"
  logger.info(s"calculated in ${calcTime}s")
  logger.info(s"total number of unique configurations: " + layouts.size)

  val task: Task[String] = Task({if (layouts.size > 10000) layouts.take(10000) else layouts} map(_.asString)
    mkString(s"Total number of unique configurations is: ${layouts.size}\ncalculated in ${calcTime}s\n\n", "\n",
    {if (layouts.size > 10000) s"\n\n ... and ${layouts.size - 10000} additional configurations." else "\n"}))
  val t = Process.eval(task) pipe { text.utf8Encode } to { io fileChunkW filename }
  t.run.run
  logger.info(s"computation results are saved into the file $filename")
}
